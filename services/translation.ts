export const translateService = async (text: string, language: string) => {
    return  await fetch("https://libretranslate.de/translate", {
        body: `q=${text}&source=en&target=${language}&format=text`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        method: "POST",
      });
  };