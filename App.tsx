import React, { useEffect, useState } from "react";
import { SafeAreaView, ScrollView, StyleSheet } from "react-native";
import useSWR from "swr";
import { useCallback } from "react";
import Header from "./components/Header";
import QuoteCard from "./components/QuotesCard";
import Translation from "./components/Translation";
import { translateService } from "./services/translation";
import { fetcher } from "./utils";

type Translation = {
  translatedText: string;
};
const App = () => {

  const breakingBagQuotesApiKey = "https://api.breakingbadquotes.xyz/v1/quotes";
  const { data: breakingBadQuote } = useSWR(breakingBagQuotesApiKey, fetcher);
  const [selectedLanguage, setSelectedLanguage] = useState<string>("fr");
  const [translatedQuote, setTranslatedQuote] = useState<Translation>();
  const translate = useCallback(async () => {
    if (breakingBadQuote?.[0]?.quote && selectedLanguage) {
      setTranslatedQuote(null);
      try {
        const response = await translateService(
          breakingBadQuote?.[0]?.quote,
          selectedLanguage
        );
        const translatedText = await response.json();
        return setTranslatedQuote(translatedText);
      } catch (error) {
        return;
      }
    }
  }, [breakingBadQuote?.[0]?.quote, selectedLanguage]);
  useEffect(() => {
    translate();
  }, [translate]);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Header />
        <QuoteCard
          quote={breakingBadQuote?.[0]?.quote ?? "Loading ..."}
          author={breakingBadQuote?.[0]?.author ?? "Loading ..."}
        />
        <Translation
          selectedLanguage={selectedLanguage}
          setSelectedLanguage={setSelectedLanguage}
          translate={translate}
          breakingBagQuotesApiKey={breakingBagQuotesApiKey}
        />
        <QuoteCard quote={translatedQuote?.translatedText ?? "Loading ..."} />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ced4da",
  },
});
export default App;
