# Breaking Quotes - Une application de citations de Breaking Bad



Breaking Quotes est une application de citations inspirées de la célèbre série télévisée Breaking Bad. L'application est développée en utilisant React Native et Expo pour faciliter le développement multi-plateforme.

## Fonctionnalités

- [x] Affichage aléatoire de citations de la série Breaking Bad
- [x] Traduire les citations dans la langue choisie par l'utilisateur
- [ ] Partage de citations avec des amis via les réseaux sociaux ou par message texte
- [ ] Enregistrement de citations préférées pour un accès rapide et facile

## Installation

Clonez le dépôt avec: 

    git clone https://github.com/votre-utilisateur/BreakingQuotes.git


 Installez les dépendances en exécutant npm install

    yarn install


Lancer le projet 


    yarn expo start


## Auteur

Cette application a été développée par Djoton Noé SOSSOU. Vous pouvez me contacter par mail djroton-noe.sossou@epitech.eu


## Licence

Ce projet est sous licence MIT. Voir le fichier LICENSE pour plus d'informations.

Cette application est un projet personnel et n'est pas affiliée à ou approuvée par la série télévisée Breaking Bad ou son équipe de production.
