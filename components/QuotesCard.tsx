import React from "react";
import { View, Text, StyleSheet } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

interface QuoteCardProps {
  quote: string;
  author?: string;
}

const QuoteCard: React.FC<QuoteCardProps> = ({ quote, author }) => {
  return (
    <View style={styles.card}>
      <Text style={styles.quoteText}>
        <MaterialCommunityIcons
          name="format-quote-open"
          size={34}
          color="#212529"
        />{" "}
        {quote}
      </Text>
      <Text style={styles.authorText}>- {author}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    marginHorizontal: 10,
    marginTop: 20,
    paddingVertical: 20,
    marginVertical: 10,
    borderColor: "#212529",
    borderTopWidth: 5,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderWidth: 2,
    backgroundColor: "#f8f9fa",
    padding: 10,
  },
  quoteText: {
    color: "#212529",
    fontSize: 15,
    textAlign: "justify",
    fontStyle: "italic",
  },
  authorText: {
    color: "#212529",
    fontWeight: "bold",
    fontSize: 15,
    marginTop: 5,
    alignSelf: "flex-end",
  },
});

export default QuoteCard;
