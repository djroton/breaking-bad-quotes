import React from "react";
import { Text, View, StatusBar, StyleSheet, Image } from "react-native";

const Header = () => {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={"#ced4da"} />
      <Image
        source={require("../assets/breaking-bad.png")}
        style={styles.image}
      />
      <Text style={styles.text}>Breaking Bad Quotes</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginTop: 15,
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    width: 60,
    height: 60,
    marginRight: 10,
  },
  text: {
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "bold",
    color: "#212529",
    textAlign: "center",
  },
});
export default Header;
