import React from "react";
import { View, StyleSheet, Pressable } from "react-native";
import { Picker } from "@react-native-picker/picker";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { mutate } from "swr";
import { LANGUAGES } from "../constants";

interface Props {
  selectedLanguage: string;
  setSelectedLanguage: (value: string) => void;
  translate: () => void;
  breakingBagQuotesApiKey: string;
}

const Translation: React.FC<Props> = ({
  selectedLanguage,
  setSelectedLanguage,
  translate,
  breakingBagQuotesApiKey,
}) => {
  return (
    <View>
      <View style={styles.container}>
        {
          <Picker
            selectedValue={selectedLanguage}
            onValueChange={(itemValue) => setSelectedLanguage(itemValue)}
          >
            {LANGUAGES?.map((item) => (
              <Picker.Item
                key={item?.code}
                label={item?.name}
                value={item?.code}
              />
            ))}
          </Picker>
        }
      </View>
      <View style={styles.buttonContainer}>
        <Pressable style={styles.button} onPress={() => translate()}>
          <MaterialCommunityIcons
            name="translate"
            size={24}
            style={styles.icon}
          />
        </Pressable>

        <Pressable
          style={styles.button}
          onPress={() => mutate(breakingBagQuotesApiKey)}
        >
          <MaterialCommunityIcons name="reload" size={24} style={styles.icon} />
        </Pressable>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginTop: 20,
    borderColor: "#212529",
    borderWidth: 1,
    borderTopWidth: 5,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    backgroundColor: "#f8f9fa",
  },
  buttonContainer: {
    marginHorizontal: 10,
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    paddingHorizontal: 20,
  },
  icon: {
    color: "#212529",
  },
});

export default Translation;
